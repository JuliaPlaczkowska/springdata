package pwr.io.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pwr.io.service.Product;

@Repository
public interface ProductRepo extends CrudRepository<Product, Long> {

}
