package pwr.io.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pwr.io.service.Customer;

@Repository
public interface CustomerRepo extends CrudRepository<Customer, Long> {

}
