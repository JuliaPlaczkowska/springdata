package pwr.io.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import pwr.io.security.user.User;
import pwr.io.security.user.UserDto;
import pwr.io.security.user.UserDtoBuilder;
import pwr.io.service.Customer;
import pwr.io.service.Order;
import pwr.io.service.Product;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {
    private ProductRepo productRepository;
    private OrderRepo orderRepository;
    private CustomerRepo customerRepository;
    private UserRepo userRepository;

    @Autowired
    public DbMockData(ProductRepo productRepository, OrderRepo orderRepository, CustomerRepo customerRepository, UserRepo userRepository) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
        this.userRepository = userRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() {



        Product product = new Product("Mleko", (float) 2.5, true);
        Product product1 = new Product("Woda", (float) 2.0, true);
        Product product2 = new Product("Cola", (float) 5.5, true);
        Customer customer = new Customer("Paweł Biłosz", "Lublin");

        Set<Product> products = new HashSet<>() {
            {
                add(product);
                add(product1);
                add(product2);
            }};

        Order order = new Order(customer, products, LocalDateTime.now(), "in progress");

        UserDto user = new UserDtoBuilder(new User("ania", "123", "ROLE_CUSTOMER")).getUserDto();
        UserDto admin = new UserDtoBuilder(new User("admin", "123", "ROLE_ADMIN")).getUserDto();

        productRepository.save(product);
        productRepository.save(product1);
        productRepository.save(product2);
        customerRepository.save(customer);
        orderRepository.save(order);
        userRepository.save(user);
        userRepository.save(admin);
    }
}