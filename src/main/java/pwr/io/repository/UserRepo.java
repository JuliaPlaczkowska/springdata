package pwr.io.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pwr.io.security.user.UserDto;


@Repository
public interface UserRepo extends CrudRepository<UserDto, Long> {

}
