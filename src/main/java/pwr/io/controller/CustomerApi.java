package pwr.io.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pwr.io.service.Customer;
import pwr.io.service.CustomerManager;

import java.util.Optional;

@RestController
@RequestMapping
public class CustomerApi {

    private CustomerManager customers;

    @Autowired
    public CustomerApi(CustomerManager customers) {
        this.customers = customers;
    }

    @GetMapping("api/customer/all")
    public Iterable<Customer> getAllCustomers() {
        return customers.findAll();
    }

    @GetMapping("api/customer")
    public Optional<Customer> getCustomerById(@RequestParam Long index) {
        return customers.findById(index);
    }

    @PostMapping("api/admin/customer")
    public Customer addCustomer(@RequestBody Customer customer ){
        return  customers.save(customer);
    }

    @PutMapping("api/admin/customer")
    public Customer updateCustomer(@RequestBody Customer customer ){
        return  customers.save(customer);
    }

    @PatchMapping("api/admin/customer")
    public Customer partlyUpdateCustomer(@RequestBody Customer customer ){
        return  customers.save(customer);
    }
}
