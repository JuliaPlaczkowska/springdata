package pwr.io.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pwr.io.service.Order;
import pwr.io.service.OrderManager;

import java.util.Optional;

@RestController
@RequestMapping
public class OrderApi {

    private OrderManager orders;

    @Autowired
    public OrderApi(OrderManager orders) {
        this.orders = orders;
    }

    @GetMapping("api/order/all")
    public Iterable<Order> getAllOrders() {
        return orders.findAll();
    }

    @GetMapping("api/order")
    public Optional<Order> getOrderById(@RequestParam Long index) {
        return orders.findById(index);
    }

    @PostMapping("api/order")
    public Order addOrder(@RequestBody Order order ){
        return  orders.save(order);
    }

    @PutMapping("/api/admin/order")
    public Order updateOrder(@RequestBody Order order ){
        return  orders.save(order);
    }

    @PatchMapping("/api/admin/order")
    public Order partlyUpdateCustomer(@RequestBody Order order ){
        return  orders.save(order);
    }
}

