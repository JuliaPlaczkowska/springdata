package pwr.io.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pwr.io.service.Product;
import pwr.io.service.ProductManager;

import java.util.Optional;

@RestController
@RequestMapping
public class ProductApi {

    private ProductManager products;

    @Autowired
    public ProductApi(ProductManager products) {
        this.products = products;
    }

    @GetMapping("/api/product/all")
    public Iterable<Product> getAllProducts() {
        return products.findAll();
    }

    @GetMapping("/api/product")
    public Optional<Product> getProductById(@RequestParam Long index) {
        return products.findById(index);
    }

    @PostMapping("/api/admin/product")
    public Product addProduct(@RequestBody Product product ){
        return  products.save(product);
    }

    @PutMapping("/api/admin/product")
    public Product updateProduct(@RequestBody Product product ){
        return  products.save(product);
    }

    @PatchMapping("/api/admin/product")
    public Product partlyUpdateProduct(@RequestBody Product product ){
        return  products.save(product);
    }
}
