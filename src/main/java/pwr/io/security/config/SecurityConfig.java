package pwr.io.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import pwr.io.security.user.PasswordEncodingConfig;

import javax.sql.DataSource;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    private PasswordEncoder passwordEncoder = new PasswordEncodingConfig().passwordEncoder();


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.jdbcAuthentication()
                .usersByUsernameQuery("SELECT u.name, u.password_hash, 1 FROM USER_DTO u WHERE u.name=?")
                .authoritiesByUsernameQuery("SELECT u.name, u.role, 1 FROM USER_DTO u WHERE u.name=?")
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()

                .antMatchers(HttpMethod.GET,"/api/product").permitAll()
                .antMatchers(HttpMethod.GET,"/api/product/all").hasAnyAuthority("ROLE_ADMIN","ROLE_CUSTOMER")
                .antMatchers(HttpMethod.GET,"/api/order").hasAnyAuthority("ROLE_ADMIN", "ROLE_CUSTOMER")
                .antMatchers(HttpMethod.GET,"/api/order/all").hasAnyAuthority("ROLE_ADMIN", "ROLE_CUSTOMER")
                .antMatchers(HttpMethod.POST,"/api/order").hasAnyAuthority("ROLE_ADMIN", "ROLE_CUSTOMER")

                .antMatchers(HttpMethod.GET,"/api/customer").hasAuthority("ROLE_CUSTOMER")
                .antMatchers(HttpMethod.GET,"/api/customer/all").hasAuthority("ROLE_CUSTOMER")

                .antMatchers(HttpMethod.POST,"/api/admin/product").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/admin/product").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.PATCH,"/api/admin/product").hasAuthority("ROLE_ADMIN")

                .antMatchers(HttpMethod.POST,"/api/admin/customer").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/admin/customer").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.PATCH,"/api/admin/customer").hasAuthority("ROLE_ADMIN")

                .antMatchers(HttpMethod.PUT,"/api/admin/customer").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.PATCH,"/api/admin/customer").hasAuthority("ROLE_ADMIN")

                .antMatchers("/h2-console/**").permitAll()
                .anyRequest().authenticated().and().httpBasic()
                .and().csrf().disable()

                .headers().frameOptions().disable();


    }
}
