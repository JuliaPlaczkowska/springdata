package pwr.io.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pwr.io.repository.UserRepo;
import pwr.io.security.user.User;
import pwr.io.security.user.UserDetailsConfig;
import pwr.io.security.user.UserDto;
import pwr.io.security.user.UserDtoBuilder;

@Service
public class UserManager implements UserDetailsService {

    private UserRepo userRepo;

    @Autowired
    public UserManager(UserRepo userRepo){
        this.userRepo = userRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        Iterable<UserDto> users = userRepo.findAll();

        UserDto user = null;

        for(UserDto u:users)
            if(u.getName().equals(s))
                user =  u;

        return  new UserDetailsConfig(user);
    }


    public UserDto save(User user){
        UserDto userDto = new UserDtoBuilder(user).getUserDto();
        return userRepo.save(userDto);
    }

}
