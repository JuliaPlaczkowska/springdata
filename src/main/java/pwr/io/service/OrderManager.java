package pwr.io.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pwr.io.repository.OrderRepo;

import java.util.Optional;

@Service
public class OrderManager {

    private OrderRepo orderRepo;

    @Autowired
    public OrderManager(OrderRepo orderRepo) {
        this.orderRepo = orderRepo;

    }

    public  Optional<Order> findById(Long id){
        return orderRepo.findById(id);
    }

    public Iterable<Order> findAll(){
        return orderRepo.findAll();
    }

    public Order save(Order order){
        return orderRepo.save(order);
    }
}

